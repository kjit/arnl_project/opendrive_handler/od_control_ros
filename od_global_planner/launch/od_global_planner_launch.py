import os
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration
from launch_ros.substitutions import FindPackageShare
from launch.actions import DeclareLaunchArgument

def generate_launch_description():

    od_display_pkg = FindPackageShare(package='od_map_display').find('od_map_display')

    use_sim_time = LaunchConfiguration('use_sim_time')
    maps_filename = LaunchConfiguration('maps_filename')

    declare_use_sim_time_cmd = DeclareLaunchArgument(
        name='use_sim_time',
        default_value='True',
        description='Use simulation (Gazebo/Carla) clock if true')

    declare_maps_filename_cmd = DeclareLaunchArgument(
        name='maps_filename',
        default_value=os.path.join(od_display_pkg, "maps/smart_city_202210_v1/smart_city_202210_v1"),
        description='Use simulation (Gazebo/Carla) clock if true')

    start_path_node = Node(
        name='global_planner',
        namespace='lexus2',
        package='od_global_planner',
        executable='GlobalPlanner',
        output='screen',
        parameters=[{'use_sim_time': use_sim_time, 'maps_filename': maps_filename}]
    )

    ld = LaunchDescription()

    ld.add_action(declare_use_sim_time_cmd)
    ld.add_action(declare_maps_filename_cmd)
    ld.add_action(start_path_node)

    return ld
