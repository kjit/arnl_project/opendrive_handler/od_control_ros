#include "od_global_planner/path_publisher_node.h"

using std::placeholders::_1;

PathPublisherNode::PathPublisherNode() : Node("PathPublisher"), count_(0)
{
    this->path_vis_publisher = this->create_publisher<visualization_msgs::msg::MarkerArray>("global/vis/path", 10);
    this->route_publisher = this->create_publisher<planner_custom_msgs::msg::Route>("global/route", 10);

    this->timer_ = this->create_wall_timer(500ms, std::bind(&PathPublisherNode::timer_callback, this));

    this->init_pose_sub = this->create_subscription<geometry_msgs::msg::PoseWithCovarianceStamped>(
        "/initialpose", 10, std::bind(&PathPublisherNode::initPoseCallback, this, _1));
    this->goal_pose_sub = this->create_subscription<geometry_msgs::msg::PoseStamped>(
        "/goal_pose", 10, std::bind(&PathPublisherNode::goalPoseCallback, this, _1));

    this->path_lanes_msg = visualization_msgs::msg::MarkerArray();

    this->car_pos_timer = this->create_wall_timer(100ms, std::bind(&PathPublisherNode::car_pos_timer_callback, this));

    this->tfBuffer = std::make_unique<tf2_ros::Buffer>(this->get_clock());
    this->tfListener = std::make_shared<tf2_ros::TransformListener>(*(this->tfBuffer));
}

void PathPublisherNode::timer_callback() {
    this->path_vis_publisher->publish(this->path_lanes_msg);
    this->route_publisher->publish(this->route_msg);
    this->route_msg.new_route.data = false;
}

void PathPublisherNode::car_pos_timer_callback()
{
    std::chrono::duration<long double> tf_time = 50ms;
    auto now = this->get_clock()->now();
    if (this->tfBuffer->canTransform("lexus2/map_viz", "lexus2/base_link", now, rclcpp::Duration(tf_time))) {
        auto transformation = this->tfBuffer->lookupTransform("lexus2/map_viz", "lexus2/base_link", now, rclcpp::Duration(tf_time));
        auto pos = transformation.transform.translation;

        this->position.position.x = pos.x;
        this->position.position.y = pos.y;
        this->position.position.z = pos.z;
        //cout << "LocalPlanner car pos: " << pos.x << ", " << pos.y << ", " << pos.z << endl;
    }
}

void PathPublisherNode::createOdrMap(double eps)
{
    this->eps = eps;

    this->declare_parameter("maps_filename", "default");
    this->get_parameter<std::string>("maps_filename", this->xodr_file);

    this->odr_map = new odr::OpenDriveMap(this->xodr_file + ".xodr", true, true, true, true, false, false, true);
}

void PathPublisherNode::initPoseCallback(const geometry_msgs::msg::PoseWithCovarianceStamped msg) {
    this->init_pos = msg.pose.pose;
}

void PathPublisherNode::goalPoseCallback(const geometry_msgs::msg::PoseStamped msg)
{
    RCLCPP_INFO(this->get_logger(), "\n---------------------------------------------------------------------");

    try {
        // car_pos is this->position.position init_pos is debug
        auto start_pos = this->position.position;
        RCLCPP_INFO(this->get_logger(), "from: '%lf', '%lf'", start_pos.x, start_pos.y);
        auto [start, start_s] = this->odr_map->find_lane(start_pos.x, start_pos.y, this->eps);
        RCLCPP_INFO(this->get_logger(), "from: '%s', '%lf', '%d', '%lf'", start.road_id.c_str(), start.lanesection_s0, start.lane_id, start_s);

        auto end_pos = msg.pose.position;
        auto [goal, goal_s] = this->odr_map->find_lane(end_pos.x, end_pos.y, this->eps);
        RCLCPP_INFO(this->get_logger(), "to: '%s', '%lf', '%d', '%lf'", goal.road_id.c_str(), goal.lanesection_s0, goal.lane_id, goal_s);

        auto path = this->odr_map->shortest_path(start, goal);

        this->updatePath(path, start_s, goal_s);
        RCLCPP_INFO(this->get_logger(), "path updated");
    } catch (const exception& e) {
        RCLCPP_INFO(this->get_logger(), "error: '%s'", e.what());
    }
}


void PathPublisherNode::updatePath(std::vector<std::vector<odr::LaneKey>> lanes, double start_s, double end_s)
{
    this->path_lanes_msg.markers.clear();
    this->route_msg.positions.clear();
    this->route_msg.new_route.data = true;

    if (lanes.size() == 0)
    {
        std::cout << "Global path size empty" << std::endl;
        return;
    }

    // Add single lane
    if (lanes.size() == 1)
    {
        auto lane_key = lanes[0][0];

        // Route msg
        auto position = planner_custom_msgs::msg::OutputPosition();
        position.start_s = start_s;
        position.end_s = end_s;
        position.lanes.push_back(this->get_route_msg(lane_key));
        this->route_msg.positions.push_back(position);

        // Vis msg
        const odr::Road& road = this->odr_map->id_to_road.find(lane_key.road_id)->second;
        const odr::LaneSection& lanesec = road.s_to_lanesection.find(lane_key.lanesection_s0)->second;
        const odr::Lane& lane = lanesec.id_to_lane.find(lane_key.lane_id)->second;

        bool road_follows_dir = lane.id < 0;
        odr::Line3D points;

        if (road_follows_dir)
            points = road.get_lane_border_line_at_t(lane, start_s, end_s, 0.1, 0.5);
        else
            points = road.get_lane_border_line_at_t(lane, end_s, start_s, 0.1, 0.5);

        this->path_lanes_msg.markers.push_back(this->fill_vis_msg(points, 0, true));
        return;
    }

    int id = 0;

    // Add first lane
    auto position = planner_custom_msgs::msg::OutputPosition();
    for (size_t i = 0; i < lanes[0].size(); ++i)
    {
        auto lane_key = lanes[0][i];

        // Route msg
        position.start_s = start_s;
        position.end_s = -1;
        position.lanes.push_back(this->get_route_msg(lane_key));

        // Vis msg
        const odr::Road& road = this->odr_map->id_to_road.find(lane_key.road_id)->second;
        const odr::LaneSection& lanesec = road.s_to_lanesection.find(lane_key.lanesection_s0)->second;
        const odr::Lane& lane = lanesec.id_to_lane.find(lane_key.lane_id)->second;

        bool road_follows_dir = lane.id < 0;
        odr::Line3D points;

        if (road_follows_dir)
            points = road.get_lane_border_line_at_t(lane, start_s, road.get_lanesection_end(lanesec.s0), 0.1, 0.5);
        else {
            points = road.get_lane_border_line_at_t(lane, lanesec.s0, start_s, 0.1, 0.5);
            std::reverse(points.begin(), points.end());
        }

        this->path_lanes_msg.markers.push_back(this->fill_vis_msg(points, id++, i == 0));
    }
    this->route_msg.positions.push_back(position);

    for (size_t i = 1; i < lanes.size() - 1; ++i)
    {
        position = planner_custom_msgs::msg::OutputPosition();
        for (size_t j = 0; j < lanes[i].size(); ++j)
        {
            auto lane_key = lanes[i][j];

            // Route msg
            position.start_s = -1;
            position.end_s = -1;
            position.lanes.push_back(this->get_route_msg(lane_key));

            // Vis msg
            const odr::Road& road = this->odr_map->id_to_road.find(lane_key.road_id)->second;
            const odr::LaneSection& lanesec = road.s_to_lanesection.find(lane_key.lanesection_s0)->second;
            const odr::Lane& lane = lanesec.id_to_lane.find(lane_key.lane_id)->second;

            bool road_follows_dir = lane.id < 0;
            odr::Line3D points = road.get_lane_border_line_at_t(lane, lanesec.s0, road.get_lanesection_end(lanesec.s0), 0.1, 0.5);
            if (!road_follows_dir)
                std::reverse(points.begin(), points.end());
                
            this->path_lanes_msg.markers.push_back(this->fill_vis_msg(points, id++, j == 0));
        }
        this->route_msg.positions.push_back(position);
    }

    // Add last lane
    position = planner_custom_msgs::msg::OutputPosition();
    for (size_t i = 0; i < lanes[lanes.size() - 1].size(); ++i)
    {
        auto lane_key = lanes[lanes.size() - 1][i];

        // Route msg
        position.start_s = -1;
        position.end_s = end_s;
        position.lanes.push_back(this->get_route_msg(lane_key));

        // Vis msg
        const odr::Road& road = this->odr_map->id_to_road.find(lane_key.road_id)->second;
        const odr::LaneSection& lanesec = road.s_to_lanesection.find(lane_key.lanesection_s0)->second;
        const odr::Lane& lane = lanesec.id_to_lane.find(lane_key.lane_id)->second;

        bool road_follows_dir = lane.id < 0;
        odr::Line3D points;

        if (road_follows_dir)
            points = road.get_lane_border_line_at_t(lane, lanesec.s0, end_s, 0.1, 0.5);
        else {
            points = road.get_lane_border_line_at_t(lane, end_s, road.get_lanesection_end(lanesec.s0), 0.1, 0.5);
            std::reverse(points.begin(), points.end());
        }

        this->path_lanes_msg.markers.push_back(this->fill_vis_msg(points, id++, i == 0));
    }
    this->route_msg.positions.push_back(position);
}

visualization_msgs::msg::Marker PathPublisherNode::fill_vis_msg(odr::Line3D points, size_t id, bool main_lane) const
{
    visualization_msgs::msg::Marker marker;
    marker.header.frame_id = "lexus2/map_viz";
    marker.header.stamp = rclcpp::Time();
    marker.lifetime = rclcpp::Duration(1, 0);
    marker.id = id;
    marker.type = visualization_msgs::msg::Marker::LINE_STRIP;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.5;
    marker.scale.y = 0.5;
    marker.scale.z = 1.0;
    marker.color.a = 1.0;
    marker.color.r = main_lane ? 0.0 : 1.0;
    marker.color.g = main_lane ? 1.0 : 0.0;
    marker.color.b = 0.0;

    for (auto p : points)
    {
        auto point = geometry_msgs::msg::Point();
        point.x = p[0];
        point.y = p[1];
        point.z = p[2];
        marker.points.push_back(point);
    }

    return marker;
}

planner_custom_msgs::msg::Lane PathPublisherNode::get_route_msg(odr::LaneKey lane) const
{
    auto lane_msg = planner_custom_msgs::msg::Lane();
    lane_msg.road_id.data = lane.road_id;
    lane_msg.lane_section_id = lane.lanesection_s0;
    lane_msg.lane_id = lane.lane_id;
    return lane_msg;
}
