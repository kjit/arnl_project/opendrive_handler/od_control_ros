#include <cstdio>

#include "rclcpp/rclcpp.hpp"
#include "od_global_planner/path_publisher_node.h"

int main(int argc, char ** argv)
{
    rclcpp::init(argc, argv);

    auto node = std::make_shared<PathPublisherNode>();
    node->createOdrMap(0.1);

    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
