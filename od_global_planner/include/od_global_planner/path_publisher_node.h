#ifndef PPN_H
#define PPN_H

#include <fstream>
#include <iostream>
#include <string>
#include <chrono>
#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"
#include "geometry_msgs/msg/point.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "nav_msgs/msg/path.hpp"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"
#include "planner_custom_msgs/msg/route.hpp"
#include "OpenDriveMap.h"

using namespace std::chrono_literals;

class PathPublisherNode : public rclcpp::Node {

    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr path_vis_publisher;
    rclcpp::Publisher<planner_custom_msgs::msg::Route>::SharedPtr route_publisher;

    rclcpp::TimerBase::SharedPtr car_pos_timer;

    rclcpp::TimerBase::SharedPtr timer_;
    size_t count_;

    rclcpp::Subscription<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr init_pose_sub;
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr goal_pose_sub;

    geometry_msgs::msg::Pose position;
    geometry_msgs::msg::Pose init_pos;
    visualization_msgs::msg::MarkerArray path_lanes_msg;
    planner_custom_msgs::msg::Route route_msg;

    std::unique_ptr<tf2_ros::Buffer> tfBuffer;
    std::shared_ptr<tf2_ros::TransformListener> tfListener{nullptr};

    odr::OpenDriveMap* odr_map;
    std::string xodr_file;
    double eps;

    void timer_callback();

    void car_pos_timer_callback();

    //void updatePath(vector<tuple<vector<tuple<odr::LaneKey, odr::Line3D>>, vector<int>>> path, double end_s);
    void updatePath(std::vector<std::vector<odr::LaneKey>> lanes, double start_s, double end_s);
    visualization_msgs::msg::Marker fill_vis_msg(odr::Line3D points, size_t id, bool main_lane) const;

    planner_custom_msgs::msg::Lane get_route_msg(odr::LaneKey lane) const;

    void initPoseCallback(const geometry_msgs::msg::PoseWithCovarianceStamped msg);

    void goalPoseCallback(const geometry_msgs::msg::PoseStamped msg);

public:
    PathPublisherNode();

    void createOdrMap(double eps);
};

#endif
