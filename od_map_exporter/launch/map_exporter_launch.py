import os
from launch import LaunchDescription
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():

    map_export_node = Node(
        name='map_exporter',
        package='od_map_exporter',
        executable='ExportPublisher',
        output='screen',
        parameters=[{'maps_filename': "/workspace/src/ros2_lexus/od_control_ros/od_map_display/maps/smart_city_v66/smart_city_v66"}]
    )

    ld = LaunchDescription()

    ld.add_action(map_export_node)

    return ld
