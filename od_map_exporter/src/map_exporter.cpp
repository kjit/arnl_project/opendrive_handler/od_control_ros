#include <cstdio>

#include "rclcpp/rclcpp.hpp"
#include "od_map_exporter/map_exporter_node.h"

int main(int argc, char ** argv)
{
    rclcpp::init(argc, argv);

    auto node = std::make_shared<MapExporterNode>();
    node->createOdrMap(0.9);
    node->exportFiles();

    //rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
