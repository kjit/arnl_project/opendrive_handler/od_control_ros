#include "od_map_exporter/map_exporter_node.h"

void MapExporterNode::createOdrMap(double eps) {

    this->eps = eps;

    this->declare_parameter("maps_filename", "default");
    this->get_parameter<std::string>("maps_filename", this->mesh_file);

    std::string xodr_file = this->mesh_file + ".xodr";
    std::ifstream file(xodr_file);
    this->file_available = file.good();
    if (!file.good()) {
        RCLCPP_ERROR(this->get_logger(), "File not found: '%s'", xodr_file.c_str());
    }

    this->ord_map = new odr::OpenDriveMap(xodr_file, true, true, true, true, false, false, true);
}

void MapExporterNode::exportFiles() {

    if (!this->file_available)
        return;

    std::cout << "Mesh exporting start" << std::endl;

    auto rnmesh = this->ord_map->get_road_network_mesh(this->eps);

    std::cout << "Mesh generated" << std::endl;

    std::ofstream lane_out_file(this->mesh_file + "_lane.obj", std::ofstream::out | std::ofstream::trunc);
    lane_out_file << rnmesh.lanes_mesh.get_obj() << std::endl;
    lane_out_file.close();

    std::ofstream roadmark_out_file(this->mesh_file + "_roadmarks.obj", std::ofstream::out | std::ofstream::trunc);
    roadmark_out_file << rnmesh.roadmarks_mesh.get_obj() << std::endl;
    roadmark_out_file.close();

    std::ofstream roadobject_out_file(this->mesh_file + "_roadobjects.obj", std::ofstream::out | std::ofstream::trunc);
    roadobject_out_file << rnmesh.road_objects_mesh.get_obj() << std::endl;
    roadobject_out_file.close();

    std::ofstream roadsignal_out_file(this->mesh_file + "_roadsignals.obj", std::ofstream::out | std::ofstream::trunc);
    roadsignal_out_file << rnmesh.road_signals_mesh.get_obj() << std::endl;
    roadsignal_out_file.close();

    std::ofstream offs_out_file(this->mesh_file + "_offs.txt", std::ofstream::out | std::ofstream::trunc);
    offs_out_file << "x_offs: " << std::setprecision (20) << this->ord_map->x_offs << std::endl;
    offs_out_file << "y_offs: " << this->ord_map->y_offs << std::endl;
    offs_out_file.close();

    std::cout << "Mesh exporting done" << std::endl;
}