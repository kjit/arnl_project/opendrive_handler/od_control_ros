#ifndef MEN_H
#define MEN_H

#include <fstream>
#include <iostream>
#include <string>
#include <chrono>
#include <functional>
#include <memory>
#include <iomanip>

#include "rclcpp/rclcpp.hpp"
#include "OpenDriveMap.h"

using namespace std::chrono_literals;

class MapExporterNode : public rclcpp::Node {

    odr::OpenDriveMap* ord_map;
    std::string mesh_file;
    double eps;
    bool file_available;

public:
    MapExporterNode() : rclcpp::Node("MapExporter") {}

    void createOdrMap(double eps);

    void exportFiles();
};

#endif
