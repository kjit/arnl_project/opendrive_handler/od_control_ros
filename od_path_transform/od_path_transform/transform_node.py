import rclpy
from rclpy.node import Node
from tf2_ros import TransformBroadcaster
from std_msgs.msg import Float64
import math
import tf2_geometry_msgs

from geometry_msgs.msg import TransformStamped
from visualization_msgs.msg import MarkerArray, Marker

class PathTransformNode(Node):

    def __init__(self):
        super().__init__('pathtransformnode')

        self.path_sub = self.create_subscription(
            MarkerArray, '/lexus2/global/vis/path', self.path_sub_callback, 10
        )
        self.path_sub # prevent unused variable warning

        self.speed_pub = self.create_publisher(Float64, 'speedgoat/longitudinal_target_speed', 10)

        self.broadcaster = TransformBroadcaster(self)

        timer_period = 0.04
        self.timer = self.create_timer(timer_period, self.timer_callback)

        self.path_id = 0
        self.path = []
        self.frame_id = ""

    def path_sub_callback(self, msg):

        if len(msg.markers) == 0:
            return
        
        if len(self.path) > 0 and msg.markers[0].points[0].x == self.path[0].x:
            return
        
        self.frame_id = msg.markers[0].header.frame_id

        self.path.clear()
        for marker in msg.markers:
            for point in marker.points:
                self.path.append(point)

        self.path_id = 0

    def timer_callback(self):

        if len(self.path) == 0 or self.path_id >= len(self.path) - 1:
            msg = Float64()
            msg.data = 0.0
            # self.speed_pub.publish(msg)
            return

        msg = Float64()
        msg.data = 10.0
        # self.speed_pub.publish(msg)

        t = TransformStamped()
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = self.frame_id
        t.child_frame_id = 'lexus2/base_link'

        current_point = self.path[self.path_id]
        next_point = self.path[self.path_id + 1]

        t.transform.translation.x = current_point.x
        t.transform.translation.y = current_point.y
        t.transform.translation.z = current_point.z

        x = next_point.x - current_point.x
        y = next_point.y - current_point.y
        yaw_local = math.atan2(y, x)
        q = quaternion_from_euler(0, 0, yaw_local)
        
        t.transform.rotation.w = q[0]
        t.transform.rotation.x = q[1]
        t.transform.rotation.y = q[2]
        t.transform.rotation.z = q[3]

        self.path_id += 1
        self.broadcaster.sendTransform(t)

def quaternion_from_euler(roll, pitch, yaw):
    """
    Converts euler roll, pitch, yaw to quaternion (w in first place)
    quat = [w, y, z, x]
    Bellow should be replaced when porting for ROS 2 Python tf_conversions is done.
    """
    cy = math.cos(yaw * 0.5)
    sy = math.sin(yaw * 0.5)
    cp = math.cos(pitch * 0.5)
    sp = math.sin(pitch * 0.5)
    cr = math.cos(roll * 0.5)
    sr = math.sin(roll * 0.5)

    q = [0] * 4
    q[0] = cy * cp * cr + sy * sp * sr
    q[1] = cy * cp * sr - sy * sp * cr
    q[2] = sy * cp * sr + cy * sp * cr
    q[3] = sy * cp * cr - cy * sp * sr

    return q

def main(args=None):
    rclpy.init(args=args)

    rvizpose = PathTransformNode()

    rclpy.spin(rvizpose)

    rvizpose.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
