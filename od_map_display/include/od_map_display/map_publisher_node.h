#ifndef MPN_H
#define MPN_H

#include <fstream>
#include <iostream>
#include <string>
#include <chrono>
#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "OpenDriveMap.h"

using namespace std::chrono_literals;

class MapPublisherNode : public rclcpp::Node {

    rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr lane_mesh_publisher;
    rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr roadmarks_mesh_publisher;
    rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr roadobjects_mesh_publisher;
    rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr roadsignals_mesh_publisher;

    rclcpp::TimerBase::SharedPtr timer_;
    size_t count_;

    odr::OpenDriveMap* ord_map;
    std::string mesh_file;
    double eps;

    void timer_callback();

    odr::Mesh3D getRoadMesh(double eps, int mesh_name);

public:
    MapPublisherNode();

    void createOdrMap(double eps);
};

#endif
