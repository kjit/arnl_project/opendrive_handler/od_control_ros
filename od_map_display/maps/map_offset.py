import re
from decimal import Decimal

# Define offsets with high precision
x_offs = Decimal("639888.83450393017847")
y_offs = Decimal("5195157.8369358619675")

# Regex pattern to match x and y coordinates in scientific notation
pattern = r'(x|y)="(-?\d+\.\d+e[+-]?\d+)"'

# Function to modify coordinates with high precision
def adjust_coordinates(match):
    coord_type = match.group(1)  # 'x' or 'y'
    value = Decimal(match.group(2))  # Convert scientific notation string to Decimal
    
    # Subtract the corresponding offset
    if coord_type == "x":
        new_value = value - x_offs
    else:  # coord_type == "y"
        new_value = value - y_offs
    
    return f'{coord_type}="{new_value:.6e}"'  # Convert back to scientific notation

# Read the XODR file
with open("smartcity_v202501_1_6.xodr", "r") as file:
    content = file.read()

# Replace the coordinates with high precision
updated_content = re.sub(pattern, adjust_coordinates, content)

# Save the modified file
with open("smart_city_v202501_1_6_offseted.xodr", "w") as file:
    file.write(updated_content)

print("Coordinates updated successfully with high precision!")

