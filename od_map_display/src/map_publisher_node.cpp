#include "od_map_display/map_publisher_node.h"
#include "functional"

using std::placeholders::_1;

MapPublisherNode::MapPublisherNode() : Node("MapPublisher"), count_(0) {

    this->lane_mesh_publisher = this->create_publisher<visualization_msgs::msg::Marker>("lane_map", 10);
    this->roadmarks_mesh_publisher = this->create_publisher<visualization_msgs::msg::Marker>("roadmark_map", 10);
    this->roadobjects_mesh_publisher = this->create_publisher<visualization_msgs::msg::Marker>("roadobject_map", 10);
    this->roadsignals_mesh_publisher = this->create_publisher<visualization_msgs::msg::Marker>("roadsignal_map", 10);

    this->timer_ = this->create_wall_timer(500ms, std::bind(&MapPublisherNode::timer_callback, this));
}

void MapPublisherNode::timer_callback() {

    if (this->mesh_file.empty()) {
        return;
    }

    auto msg = visualization_msgs::msg::Marker();

    msg.header.frame_id = "lexus2/map_viz";
    msg.header.stamp = rclcpp::Time();
    msg.lifetime = rclcpp::Duration(1, 0);
    msg.id = 0;
    msg.type = visualization_msgs::msg::Marker::MESH_RESOURCE;
    msg.pose.position.x = 0.0;
    msg.pose.position.y = 0.0;
    msg.pose.position.z = -10.0;
    msg.pose.orientation.x = 0.0;
    msg.pose.orientation.y = 0.0;
    msg.pose.orientation.z = 0.0;
    msg.pose.orientation.w = 1.0;
    msg.scale.x = 1;
    msg.scale.y = 1;
    msg.scale.z = 1;
    msg.color.a = 1.0;
    msg.color.r = 0.188;
    msg.color.g = 0.188;
    msg.color.b = 0.188;

    msg.mesh_resource = "file://" + this->mesh_file + "_lane.obj";
    this->lane_mesh_publisher->publish(msg);

    msg.color.a = 1.0;
    msg.color.r = 1.0;
    msg.color.g = 1.0;
    msg.color.b = 1.0;
    msg.pose.position.z = 0.1;

    msg.mesh_resource = "file://" + this->mesh_file + "_roadmarks.obj";
    this->roadmarks_mesh_publisher->publish(msg);

    msg.mesh_resource = "file://" + this->mesh_file + "_roadobjects.obj";
    this->roadobjects_mesh_publisher->publish(msg);

    msg.mesh_resource = "file://" + this->mesh_file + "_roadsignals.obj";
    this->roadsignals_mesh_publisher->publish(msg);
}

void MapPublisherNode::createOdrMap(double eps) {

    this->eps = eps;

    this->declare_parameter("maps_filename", "default");
    this->get_parameter<std::string>("maps_filename", this->mesh_file);

    this->ord_map = new odr::OpenDriveMap(this->mesh_file + ".xodr", true, true, true, true, false, false, true);
}

odr::Mesh3D MapPublisherNode::getRoadMesh(double eps, int mesh_name) {

    auto rnmesh = this->ord_map->get_road_network_mesh(eps);
    switch (mesh_name)
    {
    case 1:
        return rnmesh.lanes_mesh;
        break;
    
    case 2:
        return rnmesh.roadmarks_mesh;
        break;

    case 3:
        return rnmesh.road_objects_mesh;
        break;
    
    case 4:
        return rnmesh.road_signals_mesh;
        break;
    
    }
    return rnmesh.get_mesh();
}