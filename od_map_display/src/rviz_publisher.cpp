#include <cstdio>

#include "rclcpp/rclcpp.hpp"
#include "od_map_display/map_publisher_node.h"

int main(int argc, char ** argv)
{
    rclcpp::init(argc, argv);

    auto node = std::make_shared<MapPublisherNode>();
    node->createOdrMap(0.1);

    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
