import os
from launch import LaunchDescription
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
from launch.substitutions import LaunchConfiguration
from launch.actions import ExecuteProcess, DeclareLaunchArgument

def generate_launch_description():

    pkg_rviz_vis_share = FindPackageShare(package='od_map_display').find('od_map_display')

    agent = LaunchConfiguration('agent')

    declare_agent_cmd = DeclareLaunchArgument(
        name='agent',
        default_value='lexus2',
        description='Agent name')

    start_vis_node = Node(
        name='rviz_visualization',
        package='od_map_display',
        executable='MapPublisher',
        output='screen',
        parameters=[{'maps_filename': os.path.join(pkg_rviz_vis_share, "maps/smart_city_202210_v1/smart_city_202210_v1")}]
    )

    start_tf_mappark_cmd = Node(
        namespace=agent,
        package='tf2_ros',
        executable='static_transform_publisher',
        name='transform_mappark',
        arguments=["--x", "0.0", "--y", "0.0", "--z", "200.0", "--roll", "0.0", "--pitch", "0.0", "--yaw", "0.0", "--frame-id", [agent,"/map"], "--child-frame-id", [agent,"/map_viz"]])

    start_rviz_cmd = ExecuteProcess(
        cmd=['rviz2', 
            ["-d", os.path.join(pkg_rviz_vis_share,"rviz/test.rviz")]],
        shell=True,
        output='screen'
        )

    ld = LaunchDescription()

    ld.add_action(declare_agent_cmd)

    ld.add_action(start_vis_node)
    ld.add_action(start_tf_mappark_cmd)
    ld.add_action(start_rviz_cmd)

    return ld
